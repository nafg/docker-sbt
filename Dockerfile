FROM sbtscala/scala-sbt:eclipse-temurin-jammy-17.0.10_7_1.10.1_3.4.2

ENV NODE_MAJOR=22

RUN \
  apt-get update && \
  apt-get install -y \
    build-essential \
    ca-certificates \
    curl \
    docker.io \
    docker-compose \
    docker-compose-v2 \
    g++ \
    gcc \
    gnupg \
    make \
    nodejs \
    python3-pip \
    wget && \
  mkdir -p /etc/apt/keyrings && \
  curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | \
    gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
  echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | \
    tee /etc/apt/sources.list.d/nodesource.list && \
  apt-get update && \
  apt-get install nodejs -y && \
  npm install -g yarn && \
  echo '#!/usr/bin/env sh' > /usr/local/bin/amm && \
  curl -sL https://api.github.com/repos/lihaoyi/ammonite/releases/latest \
    | grep browser_download_url \
    | grep -v bootstrap \
    | sort -V \
    | tail -n1 \
    | cut -d '"' -f 4 \
    | wget -i - -O /usr/local/bin/amm && \
  chmod +x /usr/local/bin/amm
