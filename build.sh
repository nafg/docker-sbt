## utils

error_and_exit() {
  local MESSAGE="${1}"
  local HINT="${2}"
  echo ""
  echo "🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥"
  echo ""
  echo "Error \`build.sh\`"
  echo "===================="
  echo ""
  echo "Message"
  echo "-------"
  echo "$MESSAGE"
  echo ""
  echo "Hint"
  echo "----"
  echo "$HINT"
  echo ""
  echo "/end"
  echo ""
  exit 1
}

## required environment variables

if [[ -z "$GCP_PROJECT_ID" ]]; then
  error_and_exit "\$GCP_PROJECT_ID is not set" "Did you setup a service account?"
fi

if [[ -z "$GCP_SERVICE_ACCOUNT" ]]; then
  error_and_exit "\$GCP_SERVICE_ACCOUNT is not set" "Did you setup a service account?"
fi

if [[ -z "$GCP_SERVICE_ACCOUNT_KEY_DATA" ]]; then
  error_and_exit "\$GCP_SERVICE_ACCOUNT_KEY_DATA is not set" "Did you setup a service account?"
fi



## gcloud auth and configure

gcloud auth activate-service-account --key-file $GCP_SERVICE_ACCOUNT_KEY_DATA || error_and_exit "Failed to activate service account"
gcloud config set project "$GCP_PROJECT_ID" || error_and_exit "Failed to set GCP project"

## gcloud build

LOCATION=us
REPOSITORY=gcr.io
NAME=docker-sbt
IMAGE=$LOCATION-docker.pkg.dev/$GCP_PROJECT_ID/$REPOSITORY/$NAME

gcloud builds submit -t $IMAGE || error_and_exit "Failed to deploy service"
